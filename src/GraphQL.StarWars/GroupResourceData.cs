using GraphQL.StarWars.Types;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace GraphQL.StarWars
{
    public class GroupResourceData
    {
        public GroupResourceData()
        {
            _resourceNamespaces.Add(new ResourceNamespace
            {
                Id = "1",
                Name = "CMGoals",
                Resources = new[] {"1", "2"}
            });
            _resources.Add(new Resource
            {
                Id = "1",
                Name = "Diabetes",
                ParentResourceNamespaceId = "1",
                MemberGroups = new[] {"1","2"}
            });
            _resources.Add(new Resource
            {
                Id = "2",
                Name = "Asthma",
                ParentResourceNamespaceId = "1",
                MemberGroups = new[] { "1" }
            });

            _resourceNamespaces.Add(new ResourceNamespace
            {
                Id = "2",
                Name = "Invitational",
                Resources = new[] { "3", "4" }
            });
            _resources.Add(new Resource
            {
                Id = "3",
                Name = "ProgramX",
                ParentResourceNamespaceId = "2",
                MemberGroups = new[] { "3", "4" }
            });
            _resources.Add(new Resource
            {
                Id = "4",
                Name = "ProgramY",
                ParentResourceNamespaceId = "2",
                MemberGroups = new[] { "4" }
            });

            _memberGroups.Add(new MemberGroup
            {
                Id = "1",
                Name = "CmGroup1"
            });
            _memberGroups.Add(new MemberGroup
            {
                Id = "2",
                Name = "CmGroup2"
            });
            _memberGroups.Add(new MemberGroup
            {
                Id = "3",
                Name = "InvitationalGroup1"
            });
            _memberGroups.Add(new MemberGroup
            {
                Id = "4",
                Name = "InvitationalGroup2"
            });


            _members.Add(new Member
            {
                Id = "1",
                Name = "Joe",
                MemberGroups = new[] { "1", "2", "3", "4"}
            });
            _members.Add(new Member
            {
                Id = "2",
                Name = "Sue",
                MemberGroups = new[] { "2", "4" }
            });

        }

        public ResourceNamespace GetParentResourceNamespaceName(Resource resource)
        {
            return _resourceNamespaces.Where(x => x.Id == resource.ParentResourceNamespaceId).SingleOrDefault();
        }

        public IEnumerable<Resource> GetResources(ResourceNamespace resourceNamespace)
        {
            var result = new List<Resource>();
            _resources.Where(x => resourceNamespace.Resources.Contains(x.Id)).Apply(result.Add);
            return result;
        }

        public IEnumerable<Resource> GetResources(MemberGroup memberGroup)
        {
            var result = new List<Resource>();
            _resources.Where(x => x.MemberGroups.Contains(memberGroup.Id)).Apply(result.Add);
            return result;
        }

        public IEnumerable<Resource> GetResources(Member member)
        {
            var result = new List<Resource>();
            foreach (var resource in _resources)
            {
                foreach (var memberGroupId in resource.MemberGroups)
                {
                    if (member.MemberGroups.Contains(memberGroupId))
                        result.Add(resource);
                }
            }
            return result.Distinct();
        }

        public IEnumerable<Resource> GetResources(string id, string resoureceNamespace)
        {
            var member = GetMemberByIdAsync(id).Result;
            var allMemberResources = GetResources(member);
            foreach (var r in allMemberResources)
            {
                r.ParentResourceNamespace = GetParentResourceNamespaceName(r);
            }
            var filteredResources = allMemberResources.Where(x => x.ParentResourceNamespace.Name == resoureceNamespace);
            return filteredResources;
        }

        public IEnumerable<MemberGroup> GetMemberGroups(Resource resource)
        {
            var result = new List<MemberGroup>();
            _memberGroups.Where(x => resource.MemberGroups.Contains(x.Id)).Apply(result.Add);
            return result;
        }

        public IEnumerable<MemberGroup> GetMemberGroups(Member member)
        {
            var result = new List<MemberGroup>();
            _memberGroups.Where(x => member.MemberGroups.Contains(x.Id)).Apply(result.Add);
            return result;
        }

        public Task<ResourceNamespace> GetResourceNamespaceByNameAsync(string name)
        {
            return Task.FromResult(_resourceNamespaces.FirstOrDefault(x => x.Name == name));
        }

        public Task<MemberGroup> GetMemberGroupByNameAsync(string name)
        {
            return Task.FromResult(_memberGroups.FirstOrDefault(x => x.Name == name));
        }

        public Task<Resource> GetResourceByNameAsync(string name)
        {
            return Task.FromResult(_resources.FirstOrDefault(x => x.Name == name));
        }

        public Task<Member> GetMemberByIdAsync(string id)
        {
            return Task.FromResult(_members.FirstOrDefault(x => x.Id == id));
        }

        private List<Member> _members = new List<Member>();
        private List<MemberGroup> _memberGroups = new List<MemberGroup>();
        private List<Owner> _memberGroupOwners = new List<Owner>();
        private List<Resource> _resources = new List<Resource>();
        private List<ResourceNamespace> _resourceNamespaces = new List<ResourceNamespace>();
    }
}
