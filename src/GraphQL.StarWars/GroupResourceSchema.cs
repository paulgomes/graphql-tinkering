using System;
using GraphQL.Types;

namespace GraphQL.StarWars
{
    public class GroupResourceSchema : Schema
    {
        public GroupResourceSchema(Func<Type, GraphType> resolveType)
            : base(resolveType)
        {
            Query = (GroupResourceQuery)resolveType(typeof(GroupResourceQuery));
        }
    }
}
