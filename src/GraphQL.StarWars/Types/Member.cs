using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQL.StarWars.Types
{
    public class Member
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string[] MemberGroups { get; set; }
        public string[] Resources { get; set; }
    }
    public class MemberType : ObjectGraphType<Member>
    {
        public MemberType(GroupResourceData data)
        {
            Name = "Member";
            Description = "A member.";

            Field(d => d.Id).Description("The id of the member.");
            Field(d => d.Name, nullable: true).Description("The name of the member.");
            Field<ListGraphType<MemberGroupType>>("memberGroups", resolve: context => data.GetMemberGroups(context.Source))
                .Description = "The groups to which the member belongs.";
            Field<ListGraphType<ResourceType>>("resources", resolve: context => data.GetResources(context.Source))
                .Description = "The resouces to which the member is associated.";
        }
    }
}
