using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQL.StarWars.Types
{
    public class MemberGroup
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string[] Resources { get; set; }
    }
    public class MemberGroupType : ObjectGraphType<MemberGroup>
    {
        public MemberGroupType(GroupResourceData data)
        {
            Name = "MemberGroup";
            Description = "A group of members.";

            Field(d => d.Id).Description("The id of the member group.");
            Field(d => d.Name, nullable: true).Description("The name of the member group.");
            Field<ListGraphType<ResourceType>>("resources", resolve: context => data.GetResources(context.Source))
                .Description = "The resources associated with the member group.";
        }
    }
}
