using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQL.StarWars.Types
{
    public class Resource
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ParentResourceNamespaceId { get; set; }
        public ResourceNamespace ParentResourceNamespace { get; set; }
        public string[] MemberGroups { get; set; }
    }
    public class ResourceType : ObjectGraphType<Resource>
    {
        public ResourceType(GroupResourceData data)
        {
            Name = "Resource";
            Description = "A resource to be associated with a member group.";

            Field(d => d.Id).Description("The id of the resource.");
            Field(d => d.Name, nullable: true).Description("The name of the resource.");
            Field<ResourceNamespaceType>("namespace", resolve: context => data.GetParentResourceNamespaceName(context.Source))
                .Description = "The namespace to which the resource belongs";
            Field<ListGraphType<MemberGroupType>>("memberGroups", resolve: context => data.GetMemberGroups(context.Source))
                .Description = "The member groups to which the resource is associated.";
        }
    }
}
