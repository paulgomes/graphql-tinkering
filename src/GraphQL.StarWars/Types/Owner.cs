using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQL.StarWars.Types
{
    public class Owner
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
    public class OwnerType : ObjectGraphType<Owner>
    {
        public OwnerType(GroupResourceData data)
        {
            Name = "Owner";
            Description = "Owner of a resource namespace or member group.";

            Field(d => d.Id).Description("The id of owner.");
            Field(d => d.Name, nullable: true).Description("The name of the owner.");
        }
    }
}
