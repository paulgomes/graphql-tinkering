using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQL.StarWars.Types
{
    public class ResourceNamespace
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string[] Resources { get; set; }
    }
    public class ResourceNamespaceType : ObjectGraphType<ResourceNamespace>
    {
        public ResourceNamespaceType(GroupResourceData data)
        {
            Name = "ResourceNamespace";
            Description = "A namespace for resources.";

            Field(d => d.Id).Description("The id of the resource namespace.");
            Field(d => d.Name, nullable: true).Description("The name of the resource namespace.");
            Field<ListGraphType<ResourceType>>("resources", resolve: context => data.GetResources(context.Source))
                .Description = "The resources that belong to the resource namespace";
        }
    }
}
