using GraphQL.StarWars.Types;
using GraphQL.Types;

namespace GraphQL.StarWars
{
    public class GroupResourceQuery : ObjectGraphType<object>
    {
        public GroupResourceQuery(GroupResourceData data)
        {
            Name = "Query";

            Field<ResourceNamespaceType>(
                "namespace",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "name", Description = "resource namespace" }
                ),
                resolve: context => data.GetResourceNamespaceByNameAsync(context.GetArgument<string>("name")),
                description: "Get namespace by name"
            );

            Field<ResourceType>(
                "resource",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "name", Description = "name of resource" }
                ),
                resolve: context => data.GetResourceByNameAsync(context.GetArgument<string>("name")),
                description: "Get resource by name"
            );

            Field<MemberGroupType>(
                "memberGroup",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "name", Description = "name of member group" }
                ),
                resolve: context => data.GetMemberGroupByNameAsync(context.GetArgument<string>("name")),
                description: "Get member group by name"
            );

            Field<MemberType>(
                "member",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "id", Description = "id of member" }
                ),
                resolve: context => data.GetMemberByIdAsync(context.GetArgument<string>("id")),
                description: "Get member by id"
            );

            Field<ListGraphType<ResourceType>>(
                "memberResources",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "id", Description = "id of member" },
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "namespace", Description = "resource namespace" }
                ),
                resolve: context => data.GetResources(context.GetArgument<string>("id"), context.GetArgument<string>("namespace")),
                description: "Get resources for the specified memberId and namespace"
            );
        }
    }
}
